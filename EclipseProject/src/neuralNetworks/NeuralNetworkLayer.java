package neuralNetworks;

import java.util.Random;
import misc.Const;

public class NeuralNetworkLayer {
	public int nodeCount;
	public double weights 		[][];
	public double weightsMods 	[][];
	public double values 		[];
	public double desiredValues	[];
	public double errors 		[];
	public double biasValues 	[];
	public double biasWeights 	[];

	protected NeuralNetworkLayer parentLayer, childLayer;
	Random rnd;
	 
	 public NeuralNetworkLayer(int nParentNodes, int nNodes, int nChildrenNodes){
		 nodeCount = nNodes;
		 
		 parentLayer = null;
		 childLayer  = null;
		 
		 rnd = new Random();
	 }
	 
	 public void initialize(int nNodes, NeuralNetworkLayer parent, NeuralNetworkLayer child, double _weights[][], double _bias[]){
		 boolean hasChild  = child  != null;
		 int j;
		 
		 nodeCount = nNodes;
		 values 	   = new double[nodeCount];
		 desiredValues = new double[nodeCount];
		 errors		   = new double[nodeCount];
		 
		 if(parent != null)parentLayer = parent; else parentLayer = null;
		 
		 if(hasChild){
			 childLayer  = child;
			 weights 	 = new double[nodeCount][child.nodeCount];
			 weightsMods = new double[nodeCount][child.nodeCount];
			 biasValues  = new double[child.nodeCount];
			 biasWeights = new double[child.nodeCount];
		 }else{
			 childLayer  = null;
			 weights 	 = null;
			 weightsMods = null;
			 biasValues  = null;
			 biasWeights = null;
		 }
		 
		 for(int i = 0; i < nodeCount; i++){
			 values[i] 		  = 0;
			 desiredValues[i] = 0;
			 errors[i] 		  = 0;
			 if(hasChild){
				 for(j = 0; j < child.nodeCount; j++){
					 weights    [i][j] = _weights[i][j];
					 weightsMods[i][j] = _weights[i][j];
				 }
			 }
		 }
		 if(hasChild){
			 for(j = 0; j < child.nodeCount; j++){
				 biasValues	[j] = -1;
				 biasWeights[j] =  _bias[j]; 
			 }
		 } 
	 }
	 
	 public void randomizeWeights(){
		 int j;
		 for(int i = 0; i < nodeCount; i++){
			 for(j = 0; j < childLayer.nodeCount; j++){
				 weights[i][j] = getRandomWeight();
			 }
		 }
		 
		 for(j = 0; j < childLayer.nodeCount; j++){
			 biasWeights[j] = getRandomWeight();
		 }
	 }
	 
	 protected double getRandomWeight(){
		 double weight;
		 int    sign;
		 
		 weight = rnd.nextDouble();
		 sign   = rnd.nextBoolean() ? 1 : -1;
		 return weight * sign;
	 }
	 
	 @SuppressWarnings("unused")
	public void calculateValues(){
		 double x;
		 
		 if(parentLayer != null){
			 for(int i = 0; i < nodeCount; i++){
				 x = 0;
				 for(int j = 0; j < parentLayer.nodeCount; j++){
					 x += parentLayer.values[j] * parentLayer.weights[j][i];
				 }
				 x+= parentLayer.biasValues[i] * parentLayer.biasWeights[i];
				 
				 if(childLayer != null && Const.LINEAL_OUTPUT){
					 values[i] = x;
				 }else{
					 // Logistic function
					 values[i] = 1.0f / (1 + Math.exp(-x));
					 
				 }
			 }
		 }
	 }
	 
	 public void calculateError(){
		 int i;
		 double sum;
		 
		 if(childLayer == null){
			 for(i = 0; i < nodeCount; i++){
				 errors[i] = (desiredValues[i] - values[i]) * values[i] * (1.0f - values[i]);
			 }
		 }
		 
		 else if (parentLayer == null){
			 for(i = 0; i < nodeCount; i++){
				 errors[i] = 0.0f;
			 }
		 }
		 else {
			 for(i = 0; i < nodeCount; i++){
				 sum = 0;
				 for(int j = 0; j < childLayer.nodeCount; j++){
					 sum += childLayer.errors[j] * weights[i][j];
				 }
				 errors[i] = sum * values[i] * (1.0f - values[i]);
			 }
		 }
	 }
	 
	 public void adjustWeights(){
		 int j;
		 double weightMod;
		 
		 if(childLayer != null){
			 for(int i = 0; i < nodeCount; i++){
				 for(j = 0; j < childLayer.nodeCount; j++){
					 weightMod = Const.LEARNING_RATIO * childLayer.errors[j] * values[i];
					if(Const.USING_INERTIA){
						weights	   [i][j] += weightMod + Const.INERTIA_FACTOR * weightsMods[i][j];
						weightsMods[i][j]  = weightMod;
					}else{
						weights[i][j] += weightMod;
					}
				 }
			 }
			 for(j = 0; j < childLayer.nodeCount; j++){
				 weightMod = Const.LEARNING_RATIO * childLayer.errors[j] * biasValues[j];
				 biasWeights[j] += weightMod;
			 }
		 }
	 }
}
