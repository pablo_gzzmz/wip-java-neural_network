package neuralNetworks;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.text.DecimalFormat;

import misc.Const;

public class NeuralNetwork {
	String name = "Neural Network";
	
	public NeuralNetworkLayer inputLayer, hiddenLayer, outputLayer;
	
	protected static double[][] trainingPlan = {
			// 	SelfHp 	PlayerHp 	SelfCombatOutput 	PlayerCombatOuput 	Social Chasing Fleeing Herding
			{ 	0.1, 	1, 			0.1, 				0.5,				0.5, 	0.1, 	0.9, 	0.1},
			{ 	1, 		1, 			1, 					0.1,				0.5, 	0.9, 	0.1, 	0.1},
			{ 	0, 		0.1, 		0.3, 				0.5,				0.5, 	0.9, 	0.1, 	0.1},
			{ 	0.3, 	0.3, 		0.3, 				0.3,				0.5, 	0.9, 	0.1, 	0.1},
			{ 	0.5, 	0.3, 		0.3, 				0.4,				1,  	0.9, 	0.1, 	0.1},
			{ 	0.2, 	0.2, 		0.3, 				0.3,				1,  	0.9, 	0.1, 	0.1},
			{ 	1, 		1, 			0, 					0.2,				0.5, 	0.1, 	0.1, 	0.9},
			{ 	1, 		1, 			1, 					0.5,				1, 		0.1, 	0.1, 	0.9},
			{ 	0.1, 	0.2, 		0.5, 				0.3,				1, 		0.1, 	0.1, 	0.9},
			{ 	0.3, 	0.2, 		0, 					0.5,				0.2, 	0.1, 	0.9, 	0.1},
			{ 	0, 		0.2, 		0, 					0.5,				0.3, 	0.1, 	0.9, 	0.1},
			{ 	1, 		1, 			0.5, 				1,					0.2, 	0.1, 	0.1, 	0.9},
			{ 	0, 		1, 			1, 					0.5,				0.6, 	0.1, 	0.1, 	0.9},
			{ 	0, 		1, 			0, 					1,					1, 		0.1, 	0.9, 	0.1},
			{ 	0.1, 	0.2, 		0, 					0.5,				0.2, 	0.1, 	0.1, 	0.9},
			{ 	0, 		0.25, 		1, 					0.2,				0.8, 	0.1, 	0.1, 	0.9},
			{ 	0.6, 	0.1, 		1, 					0.4,				0.1, 	0.9, 	0.1, 	0.1}
	};	
	
	public int drawMode = 1;
	
	public NeuralNetwork(int nInputNodes, int nHiddenNodes, int nOutputNodes, double weights1[][], double weights2[][], double bias1[], double bias2[]){
		 inputLayer = new NeuralNetworkLayer(0, nInputNodes, nHiddenNodes);
		hiddenLayer = new NeuralNetworkLayer(nInputNodes, nHiddenNodes, nOutputNodes);
		outputLayer = new NeuralNetworkLayer(nHiddenNodes, nOutputNodes, 0);
		
		 inputLayer.initialize(nInputNodes,  null, 		  hiddenLayer, weights1, bias1);
		hiddenLayer.initialize(nHiddenNodes, inputLayer,  outputLayer, weights2, bias2);
		outputLayer.initialize(nOutputNodes, hiddenLayer, null		 , null, 	    null);
	}
	
	public void TrainNetwork () 
	{
		int i; double error = 1; int epoch = 0;
		
		while ((error > 0.05) && (epoch < 50000)) 
		{
			error = 0;
			epoch++;
			for (i = 0; i < 4; i++) 
			{
				this.setInput (0, trainingPlan [i][0]);
				this.setInput (1, trainingPlan [i][1]);
				this.setInput (2, trainingPlan [i][2]);
				this.setInput (3, trainingPlan [i][3]);
				this.setInput (4, trainingPlan [i][4]);
				this.setDesiredOutput(0, trainingPlan [i][5]);
				this.setDesiredOutput(1, trainingPlan [i][6]);
				this.setDesiredOutput(2, trainingPlan [i][7]);
				this.feedForward();
				this.backPropagation();
			}
			error += meanSquareError()/4;
		}
	}
	
	public void retrain (double[] inputs, double output0, double output1, double output2) 
	{
		double error = 1;
		int epoch = 0;

		while ((error > 0.1) && (epoch < 5000)) 
		{
			epoch++;
			this.setInput (0, inputs[0]);
			this.setInput (1, inputs[1]);
			this.setInput (2, inputs[2]);
			this.setInput (3, inputs[3]);
			this.setInput (4, inputs[4]);
			this.setDesiredOutput(0, output0);
			this.setDesiredOutput(1, output1);
			this.setDesiredOutput(2, output2);
			this.feedForward();
			this.backPropagation();
			error = meanSquareError();
		}
	}
	
	public void setName(String _name){
		name = _name;
	}
	
	public void setInput(int i, double value){
		if(i >= 0 && i < inputLayer.nodeCount)
			inputLayer.values[i] = value;
	}
	
	public double getOutput(int i){
		if(i >= 0 && i < outputLayer.nodeCount)
			return outputLayer.values[i];
		else 
			return Const.OUTPUT_ERROR;
	}
	
	public void setDesiredOutput(int i, double value){
		if(i >= 0 && i < outputLayer.nodeCount)
			outputLayer.desiredValues[i] = value;
	}
	
	public void feedForward(){
		 inputLayer.calculateValues();
		hiddenLayer.calculateValues();
		outputLayer.calculateValues();
	}
	
	public void backPropagation(){
		outputLayer.calculateError();
		hiddenLayer.calculateError();
		
		hiddenLayer.adjustWeights();
		 inputLayer.adjustWeights();
	}
	
	public int getMaxOutputId(){
		int id;
		double max;
		
		id = 0;
		max = outputLayer.values[0];
		
		for(int i = 1; i < outputLayer.nodeCount; i++){
			if(outputLayer.values[i] > max){
				id = i;
				max = outputLayer.values[i];
			}
		}
		
		return id;
	}
	
	public double meanSquareError(){
		double error = 0;
		
		for(int i = 0; i < outputLayer.nodeCount; i++){
			error += Math.pow(outputLayer.values[i] - outputLayer.desiredValues[i], 2);
		}
		
		return error / outputLayer.nodeCount;
	}
	
	public void setDrawMode(int n){
		n = n < 0 ? 0 : n > 2 ? 2 : n;
		drawMode = n;
	}
	
	public void draw(Graphics g){
		switch(drawMode){
		case 0:
			break;
		case 1:
			drawWeightData(g);
			break;
		case 2:
			drawValueData(g);
			break;
		}
	}
	
	void drawWeightData(Graphics g){
		int xpos = 20;
		int ypos = 20;
		int ysum = 20;
		int xsum = 150;
		
		int biggestCount = Math.max(inputLayer.nodeCount * hiddenLayer.nodeCount, hiddenLayer.nodeCount * outputLayer.nodeCount);
		
		g.setColor(new Color(0.2f, 0.2f, 0.2f, 0.6f));
		g.fillRect(0, 0, xsum*4, ysum * ++biggestCount + 26);
		
		g.setColor(new Color(0, 0.5f, 1f));
		g.setFont(new Font("TimesRoman", Font.BOLD, 22));
		g.drawString(name+" - CURRENT WEIGHTS", xpos, ypos);
		
		ypos += ysum;
		int initialY = ypos;
		
		DecimalFormat df = new DecimalFormat("+##,##00.00;-#");
		
		
		// Input to hidden
		g.setColor(Color.WHITE);
		g.setFont(new Font("TimesRoman", Font.BOLD, 18));
		g.drawString("Input-Hidden", xpos, ypos);
		g.setFont(new Font("TimesRoman", Font.PLAIN, 16));
		g.setColor(Color.RED);
		
		for(int i = 0; i < inputLayer.nodeCount; i++){
			for(int j = 0; j < hiddenLayer.nodeCount; j++){
				ypos += ysum;
				g.drawString(i+" :  "+df.format(inputLayer.weights[i][j])+"  : "+j, xpos, ypos);
			}
		}
		
		ypos = initialY;
		xpos += xsum;
		
		// Input - hidden bias
		for(int j = 0; j < hiddenLayer.nodeCount; j++){
			ypos += ysum;
			g.drawString(j+" :  "+df.format(inputLayer.biasWeights[j]), xpos, ypos);
		}

		ypos = initialY;
		xpos += xsum;
		
		// Hidden to output
		g.setColor(Color.WHITE);
		g.setFont(new Font("TimesRoman", Font.BOLD, 18));
		g.drawString("Hidden-Output", xpos, ypos);
		g.setFont(new Font("TimesRoman", Font.PLAIN, 16));
		g.setColor(Color.RED);
		
		for(int i = 0; i < hiddenLayer.nodeCount; i++){
			for(int j = 0; j < outputLayer.nodeCount; j++){
				ypos += ysum;
				g.drawString(i+" :  "+df.format(hiddenLayer.weights[i][j])+"  : "+j, xpos, ypos);
			}
		}

		ypos = initialY;
		xpos += xsum;
		
		// Hidden bias
		for(int j = 0; j < outputLayer.nodeCount; j++){
			ypos += ysum;
			g.drawString(j+" :  "+df.format(hiddenLayer.biasWeights[j]), xpos, ypos);
		}
	}
	
	void drawValueData(Graphics g){
		int xpos = 20;
		int ypos = 20;
		int ysum = 20;
		int xsum = 150;
		
		int biggestCount = Math.max(inputLayer.nodeCount * hiddenLayer.nodeCount, hiddenLayer.nodeCount * outputLayer.nodeCount);
		
		g.setColor(new Color(0.2f, 0.2f, 0.2f, 0.6f));
		g.fillRect(0, 0, xsum*4, ysum * ++biggestCount + 26);
		
		g.setColor(new Color(0, 0.5f, 1f));
		g.setFont(new Font("TimesRoman", Font.BOLD, 22));
		g.drawString(name+" - CURRENT VALUES", xpos, ypos);
		
		ypos += ysum;
		int initialY = ypos;
		
		DecimalFormat df = new DecimalFormat("+##,##00.00;-#");
		
		// Input
		g.setColor(Color.WHITE);
		g.setFont(new Font("TimesRoman", Font.BOLD, 18));
		g.drawString("Input", xpos, ypos);
		g.setFont(new Font("TimesRoman", Font.PLAIN, 16));
		g.setColor(Color.RED);
		
		for(int i = 0; i < inputLayer.nodeCount; i++){
			ypos += ysum;
			g.drawString(i+" :  "+df.format(inputLayer.values[i]), xpos, ypos);
		}
		
		ypos = initialY;
		xpos += xsum;
		
		// Hidden
		g.setColor(Color.WHITE);
		g.setFont(new Font("TimesRoman", Font.BOLD, 18));
		g.drawString("Hidden", xpos, ypos);
		g.setFont(new Font("TimesRoman", Font.PLAIN, 16));
		g.setColor(Color.RED);
		
		for(int i = 0; i < hiddenLayer.nodeCount; i++){
			ypos += ysum;
			g.drawString(i+" :  "+df.format(hiddenLayer.values[i]), xpos, ypos);
		}

		ypos = initialY;
		xpos += xsum;
		
		// Output
		g.setColor(Color.WHITE);
		g.setFont(new Font("TimesRoman", Font.BOLD, 18));
		g.drawString("Output", xpos, ypos);
		g.setFont(new Font("TimesRoman", Font.PLAIN, 16));
		g.setColor(Color.RED);

		for(int i = 0; i < outputLayer.nodeCount; i++){
			ypos += ysum;
			g.drawString(i+" :  "+df.format(outputLayer.values[i]), xpos, ypos);
		}

		ypos = initialY;
		xpos += xsum;
		
		// Desired
		g.setColor(Color.WHITE);
		g.setFont(new Font("TimesRoman", Font.BOLD, 18));
		g.drawString("Output", xpos, ypos);
		g.setFont(new Font("TimesRoman", Font.PLAIN, 16));
		g.setColor(Color.RED);

		for(int i = 0; i < outputLayer.nodeCount; i++){
			ypos += ysum;
			g.drawString(i+" :  "+df.format(outputLayer.desiredValues[i]), xpos, ypos);
		}
	}
}
