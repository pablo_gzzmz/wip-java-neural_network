package geneticAlgorithms;

import agents.AgentState;
import agents.EnemyAgent;
import agents.Herd;
import misc.CombatValues;
import misc.Const;
import misc.RandomGenerator;;

public class GeneticAlgorithms 
{
	static Herd herd;
	static CombatValues[] bestAptitudes;
	static AptitudeCandidate[] candidates;
	
	public GeneticAlgorithms(Herd _herd){
		herd = _herd;
		bestAptitudes = new CombatValues[Const.NUMBER_AGENTS_TO_PROCREATE];
		candidates    = new AptitudeCandidate[herd.agentsInHerd().length];
	}
	
	static CombatValues GetMostOpAgentsHerence(CombatValues previousValues){
		int index = 0;
		EnemyAgent[] group = herd.agentsInHerd();
		for(EnemyAgent agent : group){
			if(agent.ActualState() == AgentState.ALIVE)
			{
				candidates[index] = new AptitudeCandidate(index, agent.myAptitude());
			}
			else
			{
				candidates[index] = new AptitudeCandidate(index, -10);
			}
			index++;
		}
		
		candidates = SortCandidates(candidates);
		for(int indx = 0; indx < Const.NUMBER_AGENTS_TO_PROCREATE; indx++){
			bestAptitudes[indx] = new CombatValues(group[candidates[indx].index].defaultCombatValues);
		}
		
		int hp;
		if(RandomGenerator.RandomInt(0, 10) <= Const.HP_MUTABILITY){
			// Gene mutates - Limited scope based on previous values
			hp = previousValues.healthPoints;
			
			while(hp == previousValues.healthPoints){
				hp = RandomGenerator.RandomInt(previousValues.healthPoints-2, previousValues.healthPoints+2);	
			}
			if     (hp <= 0) System.out.println("hp mutated to 1");
			else if(hp >= 9) System.out.println("hp mutated to 9");
			else 			 System.out.println("hp mutated to " + hp);
		}
		else{
			// Selects one parent to pass his genes
			index = RandomGenerator.RandomInt(0, Const.NUMBER_AGENTS_TO_PROCREATE-1);
			hp = bestAptitudes[index].healthPoints;
		}
		
		
		int atckDamage;
		if(RandomGenerator.RandomInt(0, 10) <= Const.DAMAGE_MUTABILITY){
			// Gene mutates - Limited scope based on previous values
			switch(previousValues.attackDamage){
			case 0:
				atckDamage = RandomGenerator.RandomInt(1, 2);
				break;
			case 1:
				atckDamage = 1;
				while(atckDamage == 1){
					atckDamage = RandomGenerator.RandomInt(0, 3);
				}
				break;
			default:
				atckDamage = previousValues.attackDamage;
				while(atckDamage == previousValues.attackDamage){
					atckDamage = RandomGenerator.RandomInt(previousValues.attackDamage-2, previousValues.attackDamage+2);	
				}
				break;
			}
			System.out.println("atckDamage mutated to " + atckDamage);
		}
		else{
			// Selects one parent to pass his genes
			index = RandomGenerator.RandomInt(0, Const.NUMBER_AGENTS_TO_PROCREATE-1);
			atckDamage = bestAptitudes[index].attackDamage;
		}
		
		int sociability;
		if(RandomGenerator.RandomInt(0, 10) <= Const.DAMAGE_MUTABILITY){
			// Gene mutates
			sociability = RandomGenerator.RandomInt(0, 9);
			while(sociability == previousValues.sociability){
				sociability = RandomGenerator.RandomInt(0, 9);
			}
			System.out.println("atckDamage mutated to " + sociability);
		}
		else{
			// Selects one parent to pass his genes
			index = RandomGenerator.RandomInt(0, Const.NUMBER_AGENTS_TO_PROCREATE-1);
			sociability = bestAptitudes[index].attackDamage;
		}
		
		
		float radius;
		if(RandomGenerator.RandomInt(0, 10) <= Const.RADIUS_MUTABILITY){
			// Gene mutates - Limited scope based on previous genes
			radius = RandomGenerator.RandomInt((int)previousValues.combatRadius - 20, (int)previousValues.combatRadius + 20);
			// Avoid mutation from exceeding clamp
			radius = radius < 50 ? 50 : radius > 100 ? 100 : radius;
			System.out.println("radius mutated to " + radius);
		}
		else{
			// Selects one parent to pass his genes
			index = RandomGenerator.RandomInt(0, Const.NUMBER_AGENTS_TO_PROCREATE-1);
			radius = bestAptitudes[index].combatRadius;
		}
		
		
		float atckDelay;
		if(RandomGenerator.RandomInt(0, 10) <= Const.ATTACK_DELAY_MUTABILITY){
			// Gene mutates
			atckDelay = RandomGenerator.RandomFloat(0.5f, 1.5f);
			while(atckDelay == previousValues.attackDelay){
				atckDelay = RandomGenerator.RandomFloat(0.5f, 1.5f);
			}
			System.out.println("atckDelay mutated to " + atckDelay);
		}
		else
		{
			// Selects one parent to pass his genes
			index = RandomGenerator.RandomInt(0, Const.NUMBER_AGENTS_TO_PROCREATE-1);
			atckDelay = bestAptitudes[index].attackDelay;
		}
		
		
		CombatValues res = new CombatValues(hp, atckDamage, sociability, radius, atckDelay);
		return res;
		
	}
	
	static AptitudeCandidate[] SortCandidates(AptitudeCandidate[] list)
	{
		int n = list.length;
		boolean swapped = true;
		
		while(swapped)
		{
			swapped = false;
			for(int aux = 0; aux < n-1; aux++)
			{
				if(list[aux].aptitude < list[aux+1].aptitude)
				{
					list = Swap(list, aux);
					swapped = true;
				}
			}
			n--;
		}
		return list;
	}
	
	static AptitudeCandidate[] Swap(AptitudeCandidate[] list, int indexToSwap)
	{
		AptitudeCandidate aux = new AptitudeCandidate(list[indexToSwap]);
		list[indexToSwap] = new AptitudeCandidate(list[indexToSwap+1]);
		list[indexToSwap+1] = new AptitudeCandidate(aux);
		
		return list;
	}
}
