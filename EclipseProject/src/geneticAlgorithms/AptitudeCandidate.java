package geneticAlgorithms;

public class AptitudeCandidate 
{
	
	public int index;
	public float aptitude;
	
	public AptitudeCandidate(int _index, float _aptitude) 
	{
		index = _index;
		aptitude = _aptitude;
	}
	
	public AptitudeCandidate(AptitudeCandidate b) 
	{
		index = b.index;
		aptitude = b.aptitude;
	}
}
