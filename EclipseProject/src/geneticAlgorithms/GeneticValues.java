package geneticAlgorithms;

import misc.CombatValues;

public class GeneticValues {
	// Deprecated
	// Refers to the percieved combat usefullnes of the holder of Genetic values until its death
	//public int combatUtility;
	
	// Deprecated
	// Refers to the percieved combat loss of the holder of Genetic values until its death
	// public int combatFutility;
	
	protected float aptitude;
	
	public GeneticValues(){
		aptitude = 0;
	}
	
	public GeneticValues(CombatValues combatValues){
		aptitude = combatValues.healthPoints + combatValues.attackDamage + combatValues.combatRadius/50 + combatValues.attackDelay*5;
	}
	
	// Keep this method to make some changes onto aptitude if we want to latter on.
	public float calculateAptitude(){
		return aptitude;
	}
	
	// Keep this method to make some changes onto aptitude if we want to latter on.
	public void addAptitude(int amount){
		aptitude += amount;
	}
	
	// Use this after a unit has died to establish its new values
	public CombatValues evolveIndividual(CombatValues currentValues)
	{
		return GeneticAlgorithms.GetMostOpAgentsHerence(currentValues);
	}
}
