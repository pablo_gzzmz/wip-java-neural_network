package main;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter; // For keyboard input
import java.awt.event.KeyEvent; // For keyboard input
import java.awt.event.MouseAdapter; // For mouse input
import java.awt.event.MouseEvent; // For mouse input

import javax.swing.JPanel;
import javax.swing.Timer;

import agents.AgentVisionType;
import agents.ControllableAgent;
import agents.Herd;
import geneticAlgorithms.GeneticAlgorithms;
import misc.CombatValues;
import misc.Const;
import misc.MessageLog;
import misc.Utils;

// JPanel is for adding "Game" to our JFrame "Window".
// ActionListener let us make a response on events.

@SuppressWarnings("serial")
public class Game extends JPanel implements ActionListener 
{
	
	private Timer timer;
    private Map map;
    private ControllableAgent player;
    private Herd herd;
    @SuppressWarnings("unused")
	private GeneticAlgorithms ga;
    
    private boolean gameOver;
    
    private boolean paused = false;
    
    public Game() 
    {
    	gameOver = false;
    	addKeyListener(new TAdapter()); 	// For keyboard input
    	addMouseListener(new MAdapter());	// For mouse input
        setFocusable(true);				 	// Able the game to be main focus on screen
        
        timer = new Timer(Const.DELAY, this);
        timer.start();
        
        create();
    }
    
    void create(){
    	player = new ControllableAgent(new Vector2(Const.WINDOW_WIDTH*0.5f, Const.WINDOW_HEIGHT*0.5f), new Vector2(0, -1), "/resources/player.png", "/resourcesdead.png", new CombatValues(8,3,5,50.f,1.f));
    	herd = new Herd(Const.HERD_MEMBERS);
    	ga = new GeneticAlgorithms(herd);
    	map = new Map(player, herd, "/resources/scene.png");
    }
    
    void gameOver(){
    	gameOver = true;
    	map = new Map("/resources/gameOverImage.png");
    }

    @Override
    public void paintComponent(Graphics g) {	// Method called to paint the scene
        super.paintComponent(g);				

        DoDrawing(g);							// We add our paint method
        
        Toolkit.getDefaultToolkit().sync();		// Force the paint of all elements correctly. Some systems require it.
    }

    private void DoDrawing(Graphics g) {
    	map.draw(g, this);
    	
    	Herd.neuralNetwork.draw(g);
    	
    	MessageLog.draw(g);
    }

    @Override
    // Action invoked with the timer ticks. 
    public void actionPerformed(ActionEvent e) {
    	if(paused||gameOver) return;
    	map.update();
    	repaint();
    	
    	if (map.checkGameOver())	gameOver();
    }
    
    // For keyboard input
    private class TAdapter extends KeyAdapter {
    	@Override
    	public void keyReleased(KeyEvent e){
    		int key = e.getKeyCode();
    		
    		//Controllable agent
    		if (key == KeyEvent.VK_W){
    			player.setMoveForward(false);
    		}
    		else if (key == KeyEvent.VK_S){
    			player.setMoveBackward(false);
    		}
    		else if (key == KeyEvent.VK_A){
    			player.setRotateLeft(false);
    		}
    		else if (key == KeyEvent.VK_D){
    			player.setRotateRight(false);
    		}
    	}
    	@Override
    	public void keyPressed(KeyEvent e) {
    		int key = e.getKeyCode();
    		
    		//Pause
    		if (key == KeyEvent.VK_SPACE) paused = !paused;
    		
    		// Train
    		if (key == KeyEvent.VK_R) Herd.neuralNetwork.TrainNetwork();
    		
    		//Controllable agent
    		else if (key == KeyEvent.VK_W){
    			player.setMoveForward(true);
    		}
    		else if (key == KeyEvent.VK_S){
    			player.setMoveBackward(true);
    		}
    		else if (key == KeyEvent.VK_A){
    			player.setRotateLeft(true);
    		}
    		else if (key == KeyEvent.VK_D){
    			player.setRotateRight(true);
    		}
    		
    		//Neural network debug
    		else if (key == KeyEvent.VK_1){ 
    			Herd.neuralNetwork.setDrawMode(0);
    		}
    		else if (key == KeyEvent.VK_2){ 
    			Herd.neuralNetwork.setDrawMode(1);
    		}
    		else if (key == KeyEvent.VK_3){ 
    			Herd.neuralNetwork.setDrawMode(2);
    		}
    		
    		//Vision
    		else if (key == KeyEvent.VK_U) Utils.VISION_TYPE = AgentVisionType.NARROW;
    		else if (key == KeyEvent.VK_I) Utils.VISION_TYPE = AgentVisionType.LIMITED;
    		else if (key == KeyEvent.VK_O) Utils.VISION_TYPE = AgentVisionType.WIDE;
    	}
    }
    
    // For mouse input - DEJAR ESTO POR SI SE QUIERE A�ADIR ALGUNA FUNCIONALIDAD CON RATON
    private class MAdapter extends MouseAdapter{
    	@Override
    	public void mousePressed(MouseEvent e){
    		//int posX = e.getX();
    		//int posY = e.getY();
    	}
    }
}