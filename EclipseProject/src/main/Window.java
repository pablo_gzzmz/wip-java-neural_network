package main;
import javax.swing.JFrame;

import misc.Const;

@SuppressWarnings("serial")
public class Window extends JFrame 
{
	public Window() 
    {
        add(new Game());	// Executes the game
        
        setSize(Const.WINDOW_WIDTH, Const.WINDOW_HEIGHT); 	// Sets the windows size
        setResizable(false);	// Avoids the window to be sizable
        
        setTitle("Space Herd"); // Sets the name of the game.
        setLocationRelativeTo(null);	// Centers the window on the screen
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	// Sets the exit button to work as expected
    }
}