package main;
import java.awt.EventQueue;

public class Main {
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() // calls a runnable item at the end of the event queue.
		{ 	
			@Override					// Overrides the run method.
			public void run() 
			{
				Window ex = new Window();			
				ex.setVisible(true);			
			}
		});
	}
}
