package main;
import misc.Const;
import misc.RandomGenerator;

public class Vector2 {
	public float x, y;
	
	//CONSTRUCTORS
	public Vector2(){
		x = 0; y = 0;
	}
	public Vector2(float _x, float _y){
		x = _x; y = _y;
	}
	public Vector2(Vector2 other){
		x = other.x; y = other.y;
	}
	
	//BASIC FUNCTIONS
	public float magnitude(){
		return (float) Math.sqrt(x*x + y*y);
	}
	
	public void normalize(){
		float m = magnitude();
		if (m <= Const.FTRESHOLD) m = Const.FTRESHOLD;
		x /= m; y /= m;
		if(Math.abs(x) < Const.FTRESHOLD) x = 0;
		if(Math.abs(y) < Const.FTRESHOLD) y = 0;
	}
	public Vector2 newNormalized(){
		Vector2 res = new Vector2(x, y);
		res.normalize();
		return res;
	}
	
	public static Vector2 newRandomDirection(){
		int rx = 0;
		int ry = 0;
		while(rx == 0 && ry == 0){
			rx = RandomGenerator.RandomInt(-1, 1);
			ry = RandomGenerator.RandomInt(-1, 1);
		}
		return new Vector2(rx, ry);
	}
	
	//MATH FUNCTIONS
	public void invert(){
		x = -x; y = -y;
	}
	
	public Vector2 newInverted(){
		return new Vector2(-x, -y);
	}
	
	public Vector2 add (Vector2 other){
		return new Vector2(x + other.x, y + other.y);
	}
	
	public Vector2 sub (Vector2 other){
		return add(other.newInverted());
	}
	
	public Vector2 mult (float p){
		return new Vector2(x*p, y*p);
	}
	public Vector2 div (float p){
		return new Vector2(x/p, y/p);
	}
	
	public float dot(Vector2 other){
		return x * other.x + y * other.y;
	}
	
	public Vector2 rot2D (float angle){
		float newX, newY;
		angle = -angle;
		
		newX = (float) (x * Math.cos(angle) + y * Math.sin(angle));
		newY = (float) (-x * Math.sin(angle) + y * Math.cos(angle));
		return new Vector2(newX, newY);
	}
	
	public static Vector2 zero(){
		return new Vector2(0, 0);
	}
}
