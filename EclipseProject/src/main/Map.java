package main;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import agents.Agent;
import agents.AgentFaction;
import agents.ControllableAgent;
import agents.Herd;
import misc.Const;
import misc.MessageLog;
import misc.RandomGenerator;


public class Map {
	public static Agent[] allAgents;
	public static ControllableAgent player;
	public static Herd herd;
	
	public static int[] scoring;
	
	protected Image mapImage;
	
	public Map(String img){ //Only for game over purposes
		SetImage(img);
	}
	
	public Map(ControllableAgent _player, Herd _herd, String img){
		player = _player;
		herd = _herd;
		
		scoring = new int[AgentFaction.values().length];
		
		allAgents = new Agent[herd.amount()+1];
		for(int i = 0; i < herd.amount(); i++){
			allAgents[i] = (Agent)herd.agentsInHerd()[i];
			// Set player as target
			herd.agentsInHerd()[i].setTarget(player);
		}

		allAgents[herd.amount()] = player;
		
		SetImage(img);
		
		MessageLog.recieveMessage("Initilalizing");
	}
	
	private void SetImage(String img) {
		ImageIcon ii = new ImageIcon(getClass().getResource(img));
		mapImage = ii.getImage();
	}
	
	public static Vector2 getRandomPos(){
		Vector2 res = new Vector2(RandomGenerator.RandomInt(100, Const.WINDOW_WIDTH-100),RandomGenerator.RandomInt(100, Const.WINDOW_HEIGHT-100));
		return res;
	}
	
	boolean isNotInHerd(float x, float y){
		if(((herd.amount()/5)*Const.INITIAL_AGENT_SEPARATION + herd.initialPos + Const.OBSTACLE_GENERATION_SEPARATION) < x) return true;
		if(herd.initialPos - Const.OBSTACLE_GENERATION_SEPARATION > x) return true;
		
		if((5 * Const.INITIAL_AGENT_SEPARATION + herd.initialPos + Const.OBSTACLE_GENERATION_SEPARATION) < y) return true;
		if(herd.initialPos - Const.OBSTACLE_GENERATION_SEPARATION > y) return true;
		
		return false;
	}
	
	public void update(){
		for(int i = 0; i < allAgents.length; i++){
			allAgents[i].update();
		}
		
		herd.update();
		
		handleCollisions();
	}
	
	public void sumScoring(AgentFaction f){
		scoring[f.ordinal()] += 1;
	}
	
	void handleCollisions(){
		for(int i = 0; i < allAgents.length; i++){
			for(int j = 0; j < allAgents.length; j++){
				// If agents are colliding with one another
				if(allAgents[i].checkIfCollisionWith(allAgents[j].getPos())){
					
					// Process collisiong, boolean checks if a scoring sum must occur
					if(allAgents[i].processCollisionWithAgent(allAgents[j])){
						
						// If boolean passes, set scoring +1 of given faction
						sumScoring(allAgents[i].getFaction());
					}
				}
			}
		}
	}
	
	public void draw(Graphics g, JPanel jp){
		g.drawImage(mapImage, 0, 0, jp);
		
		for(int i = 0; i < allAgents.length; i++){
			allAgents[i].draw(g, jp);
		}
		for(int i = 0; i < allAgents.length; i++){
			allAgents[i].drawUI(g, jp);
		}
	}
	
	public boolean checkGameOver(){
		if(herd.herdDestroyed()) 	return true;
		else 						return false;
	}
}
