package agents;
import java.awt.Graphics;

import javax.swing.JPanel;

import main.Map;
import main.Vector2;
import misc.Const;
import neuralNetworks.NeuralNetwork;
import misc.CombatValues;

public class Herd {
	protected EnemyAgent[] group;
	protected int amount;
	public int initialPos;
	
	public static NeuralNetwork neuralNetwork;
	
	public Herd(int number){
		Create(number, 10);
	}
	
	private void Create(int number, int initialPosition) {
		amount = number;
		group = new EnemyAgent[amount];
		initialPos = initialPosition;
		
    	for(int i = 0; i < amount; i++){
    		group[i] = new EnemyAgent(new Vector2 ((i/5) * Const.INITIAL_AGENT_SEPARATION + initialPos, initialPos + (i%5) * Const.INITIAL_AGENT_SEPARATION), 
    				new Vector2(1,-1), 
    				"/resources/enemy.png", "/resources/dead.png",
    				new CombatValues()
    				);
    	}
    	
        neuralNetwork = new NeuralNetwork(5, 5, 3, Const.WEIGHTS1, Const.WEIGHTS2, Const.BIAS1, Const.BIAS2);
	}
	
	public EnemyAgent[] agentsInHerd(){
		return group;
	}
	
	public void update(){
		if (!hasAnyLeader()){
			for(int i = 0; i < amount; i++){
				if (group[i].state == AgentState.ALIVE){
					group[i].leader = true;
					group[i].procNewLeader();
					break; //Only makes the 1st alive agent the new leader
				}
			}
		}
	}

	public void draw(Graphics g, JPanel jp){
		for(int i = 0; i < amount; i++){
			group[i].draw(g, jp);
		}
	}
	
	public boolean hasAnyLeader(){
		for(int i = 0; i < amount; i++){
			if (group[i].leader == true) return true;
		}
		return false;
	}
	
	public boolean hasAnyAlive(){
		for(int i = 0; i < amount; i++){
			if (group[i].state == AgentState.ALIVE) return true;
		}
		return false;
	}
	
	public boolean herdDestroyed(){
		if(amount <= 0)	return true;
		else 			return false;
	}
	
	public int amount(){
		return amount;
	}
}
