package agents;

public enum AgentVisionType {
	WIDE,
	LIMITED,
	NARROW
}
