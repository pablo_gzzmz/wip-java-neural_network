package agents;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import main.Vector2;
import misc.Const;
import misc.CombatValues;
import misc.Utils;
import main.Map;

import java.awt.geom.AffineTransform;
import java.text.DecimalFormat;

public class Agent {
	protected Vector2 position;
	protected Vector2 fixedPosition;
	protected Vector2 direction;
	protected Image   image;
	
	protected float rotation;
	protected float imgWidth, imgHeight;
	protected float hImgWidth, hImgHeight;

	private float acceleration;
	private float stoppingAcceleration;
	private float maxSpeed;

	protected AgentState   state;
	protected AgentFaction faction;

	public CombatValues defaultCombatValues;
	public CombatValues combatValues;
	
	private static float sdelay = Const.DELAY * 0.001f;
	protected      float combatTimer;
	protected      float respawnTimer;
	
	protected       float speed;
	protected final float backFactor = 0.6f;
	
	private float stopThreshold = 0.004f;
	
	protected boolean steeringL, steeringR, movingF, movingB;
	
	static int idCount = 0;
	int id;
	
	public Agent (Vector2 startingPosition, Vector2 startingDirection, String img, CombatValues defaultValues){
		defaultCombatValues = defaultValues;
		
		initialize(startingPosition, startingDirection);
		
		changeMovementValues(1f, 0.01f, 0.05f); //Default values
		
		ImageIcon ii = new ImageIcon(getClass().getResource(img));
		image = ii.getImage();
		imgWidth = ii.getIconWidth(); 
		imgHeight = ii.getIconHeight();
		hImgWidth = imgWidth/2; 
		hImgHeight = imgHeight/2;
		
		id = idCount;
		idCount++;
	}
	
	void initialize(Vector2 pos, Vector2 dir){
		if(dir == Vector2.zero()) dir.y = 1;
		
		position = pos; 
		setFixedPosition();
		direction = dir.newNormalized();
		rotation = (float) Math.atan2(direction.y, direction.x);

		state = AgentState.ALIVE;
		
		// Avoid overriding genetic changes
		combatValues = new CombatValues();
		combatValues.changeValues(defaultCombatValues);
		combatTimer  = 0;
		respawnTimer = Const.RESPAWN_DELAY_SECONDS;
	}
	
	public AgentFaction getFaction(){ return faction;}
	public void setFaction(AgentFaction f){ faction = f; }
	
	// Calculation of approximated attack output based on three main combat values
	public static float getAttackOutput(CombatValues values){
		return values.attackDamage/2 + (1/(values.attackDelay * 4))/2 + (values.combatRadius * 0.05f)/2;
	}
	
	public void changeMovementValues(float _maxSpeed, float _acceleration, float _stoppingAcceleration){
		maxSpeed = _maxSpeed;
		acceleration = _acceleration;
		stoppingAcceleration = _stoppingAcceleration;
		stopThreshold = stoppingAcceleration * 0.9f;
	}
	
	public void update(){
		if(state == AgentState.DEATH) {
			if(respawnTimer > 0){
				respawnTimer -= sdelay;
			}else{
				respawnTimer = Const.RESPAWN_DELAY_SECONDS;
				subRespawn();
				respawn();
			}
			return;
		}
		
		setFixedPosition();
		specificUpdate();
		move();
		
		combatTimer  = combatTimer  > 0 ? combatTimer  -= sdelay : 0;
	}
	
	protected void specificUpdate(){}
	
	public boolean checkIfCollisionWith(Vector2 pos){
		float x = fixedPosition.x - pos.x;
		float y = fixedPosition.y - pos.y;
		
		// Check if in range
		if(Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)) <= combatValues.combatRadius){
			return true;
		}
		
		return false;
	}
	
	public boolean processCollisionWithAgent(Agent other){
		// If it is an enemy
		if(other.faction != faction){
			
			// And we can join combat
			if(combatTimer <= 0){
				// Set timer for delay
				combatTimer = combatValues.attackDelay;
				
				// Make enemy receive damage and check if it is a death blow
				if(dealDamage(other)){
					// Notify of killing enemy
					return true;
				}
			}
		}else{
			// POTENTIAL HEALING/SHIELDING/SUPPORT/... OF ALLIES
		}
		
		return false;
	}
	
	// Returns true if kills the enemy
	public boolean dealDamage(Agent other){
		subDealDamage(combatValues.attackDamage);
		return other.recieveDamage(combatValues.attackDamage);
	}
	
	// Override for additional damage dealing functionality
	protected void subDealDamage(int d){}
	
	// Returns true if kills the enemy
	// Overridden in EnemyAgent to change the aptitude values
	public boolean recieveDamage(int d){
		combatValues.healthPoints -= d;

		subRecieveDamage(d);
		
		if(combatValues.healthPoints <= 0){
			kill();
			return true;
		}
		return false;
	}
	
	protected void subRecieveDamage(int d){}
	
	void kill(){
		speed = 0;
		state = AgentState.DEATH;
	}
	
	void respawn(){
		initialize(Map.getRandomPos(), Vector2.newRandomDirection());
		state = AgentState.ALIVE;
	}
	
	// Override for additional respawn functionality
	protected void subRespawn(){
		
	}
	
	public Vector2 getPos(){ return fixedPosition; }

	protected void move(){
		if(steeringR){
			direction = direction.rot2D(Const.ROT_ANGLE);
		}
		if(steeringL){
			direction = direction.rot2D(-Const.ROT_ANGLE);
		}
		
		rotation = (float) Math.atan2(direction.y, direction.x);
		
		float modifier = 0;
		if(!movingF && !movingB){
			//Decelerate to stop if speed is not 0
			if(speed <= 0-stopThreshold){
				modifier = stoppingAcceleration;
			}else if(speed >= 0+stopThreshold){
				modifier = -stoppingAcceleration;
			}
			else speed = 0;
		}
		else{
			//Forward acceleration, given priority
			if(movingF){
				modifier = acceleration;
			}
			//Backward acceleration
			else if(movingB){
				modifier = -(acceleration * backFactor);
			}
		}
		
		speed = Utils.clampFloat(speed + modifier, -(maxSpeed * backFactor), maxSpeed);
		position = position.add(direction.mult(speed));
		
		OutsideWindowFix();
	}
	
	void setFixedPosition(){
		fixedPosition = new Vector2(position.x+hImgWidth, position.y+hImgHeight);
	}
	
	public void draw(Graphics g, JPanel jp){
		AffineTransform identity  = new AffineTransform();
		AffineTransform transform = new AffineTransform();
		transform.setTransform(identity);
		transform.translate(position.x, position.y);
		transform.rotate   (rotation, hImgWidth, hImgHeight);
		
		subDraw(g, jp, transform);
	}
	
	protected final static int uiCircleRadius = 14;
	protected final static int uiCircleDiam = uiCircleRadius*2;
	public void drawUI(Graphics g, JPanel jp){
		if(state == AgentState.ALIVE){
			
			// DRAW HEALTH AND ATTACK POINTS AS UI
			g.setColor(Color.RED);
			g.fillOval((int)position.x-uiCircleRadius, (int)position.y-10-uiCircleRadius, uiCircleDiam, uiCircleDiam);
			g.setColor(new Color(0.1f, 0.7f, 0.25f));
			g.fillOval((int)(position.x+imgWidth-uiCircleRadius), (int)position.y-10-uiCircleRadius, uiCircleDiam, uiCircleDiam);
			g.setColor(Color.BLACK);
			g.drawOval((int)position.x-uiCircleRadius, (int)position.y-10-uiCircleRadius, uiCircleDiam, uiCircleDiam);
			g.drawOval((int)(position.x+imgWidth-uiCircleRadius), (int)position.y-10-uiCircleRadius, uiCircleDiam, uiCircleDiam);
			g.setColor(Color.WHITE);
			g.setFont(new Font("TimesRoman", Font.BOLD, 22));
			g.drawString(""+combatValues.attackDamage, (int)position.x-5, (int)position.y-3);
			g.drawString(""+combatValues.healthPoints, (int)(position.x + imgWidth - 5), (int)position.y-3);
			
			// DRAW AREA ATTACK RADIUS
			g.setColor(Color.RED);
			g.drawOval((int)position.x-(int)combatValues.combatRadius+(int)hImgWidth, (int)position.y-(int)combatValues.combatRadius+(int)hImgHeight, (int)combatValues.combatRadius*2, (int)combatValues.combatRadius*2);
		}
		else{
			DecimalFormat df = new DecimalFormat("##0;");
			g.setColor(new Color(0.75f, 0.35f, 0f));
			g.fillOval((int)fixedPosition.x, (int)fixedPosition.y, uiCircleDiam, uiCircleDiam);
			g.setColor(Color.BLACK);
			g.drawOval((int)fixedPosition.x, (int)fixedPosition.y, uiCircleDiam, uiCircleDiam);
			g.setColor(Color.WHITE);
			g.setFont(new Font("TimesRoman", Font.BOLD, 22));
			g.drawString(df.format(respawnTimer), (int)fixedPosition.x+6, (int)fixedPosition.y+20);
		}
		
		subDrawUI(g, jp);
	}
	
	// Override this function for extra drawing after transform calculation
	protected void subDraw(Graphics g, JPanel jp, AffineTransform transform){}

	// Override this function for extra drawing after default UI draw
	protected void subDrawUI(Graphics g, JPanel jp){}
	
	void OutsideWindowFix(){
		if(position.x > Const.WINDOW_WIDTH){
			position.x = 0-imgWidth; 
		}else if(position.x+imgWidth < 0){
			position.x = Const.WINDOW_WIDTH;
		}
		
		if(position.y+imgHeight < 0){
			position.y = Const.WINDOW_HEIGHT;
		}else if(position.y > Const.WINDOW_HEIGHT){
			position.y = 0-imgHeight;
		}
	}
	
	public AgentState ActualState()
	{
		return state;
	}
}
