package agents;

public enum EnemyAgentAction {
	CHASING,
	FLEEING,
	HERDING
}
