package agents;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import main.Vector2;
import misc.CombatValues;

public class ControllableAgent extends Agent {
	
	protected Image deathImage;
	
	public ControllableAgent(Vector2 pos, Vector2 dir, String img, String deathImg, CombatValues defaultCombatValues){
		super(pos, dir, img, defaultCombatValues);
		
		ImageIcon ii = new ImageIcon(deathImg);
		deathImage = ii.getImage();
		
		changeMovementValues(1.5f, 0.015f, 0.025f); //Default values
		
		faction = AgentFaction.PLAYER;
	}
	
	/*protected void subRespawn(){
		Herd.neuralNetwork.retrain(Herd.neuralNetwork.inputLayer.values, 0.9, 0.1, 0.1);
	}*/
	
	public void subDraw(Graphics g, JPanel jp, AffineTransform transform) {
        Graphics2D g2d = (Graphics2D) g;
        switch(state){
        case ALIVE:
        	g2d.drawImage(    image, transform, jp);
        	break;
        case DEATH:
        	g2d.drawImage(deathImage, transform, jp);
        	break;
        }
	}
	
	public void setMoveForward(boolean b){
		movingF = b;
	}
	
	public void setMoveBackward(boolean b){
		movingB = b;
	}
	
	public void setRotateLeft(boolean b){
		steeringL = b;
	}
	
	public void setRotateRight(boolean b){
		steeringR = b;
	}
}
