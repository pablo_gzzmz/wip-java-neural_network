package agents;


import main.Map;
import main.Vector2;
import misc.Const;
import misc.GenericImageStorage;
import misc.CombatValues;
import misc.Utils;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import geneticAlgorithms.GeneticValues;

public class EnemyAgent extends Agent {
	protected float agentLength;
	protected boolean blockSteer = false;
	protected float totalSteer = 0;

	protected Image image2;
	protected Image icon;
	
	static Image image3;
	static Image image4;
	
	protected EnemyAgentAction enemyAction;
	
	protected boolean leader;
	protected boolean newLeader;
	protected boolean fleeHerding = false;
	
	private Agent target;
	
	private GeneticValues geneticValues;
	
	private float separationMultiplier = 1;
	private float cohesionMultiplier = 1;
	private float alignmentMultiplier = 1;
	
	// Distance considered safe by all enemy agents to stop fleeing
	// This avoids stupid behavior when on map edges
	public static final float safeDistance = 400;
	
	public EnemyAgent(Vector2 pos, Vector2 dir, String img, String img2, CombatValues defaultCombatValues){
		super(pos, dir, img, defaultCombatValues);
		agentLength = imgWidth;
		
		ImageIcon ii = new ImageIcon(img2);
		image2 = ii.getImage();
	
		ii = new ImageIcon(getClass().getResource("/resources/enemy2.png"));
		image3 = ii.getImage();
		ii = new ImageIcon(getClass().getResource("/resources/enemy3.png"));
		image4 = ii.getImage();
		
		leader = false;
		newLeader = false;
		
		faction = AgentFaction.ENEMY;
		enemyAction = EnemyAgentAction.FLEEING;
		
		geneticValues = new GeneticValues(combatValues);
	}
	
	/*protected void subRespawn(){
		defaultCombatValues.changeValues(geneticValues.evolveIndividual(defaultCombatValues));
		Herd.neuralNetwork.reentrenarRed(Herd.neuralNetwork.inputLayer.values, 0.1, 0.9, 0.1);
		super.subRespawn(); // To change the state to alive in case the override is pulled.
	}*/
	
	protected void subDealDamage(int d){
		geneticValues.addAptitude(d);
	}
	
	protected void subRecieveDamage(int d){
		geneticValues.addAptitude(-d);
	}
	
	public float myAptitude(){
		return geneticValues.calculateAptitude();
	}
	
	public void setTarget(Agent _target){
		target = _target;
	}
	
	protected void specificUpdate(){
		// Give neural network our input
		Herd.neuralNetwork.setInput(0, 		  combatValues.healthPoints*0.1);
		Herd.neuralNetwork.setInput(1, target.combatValues.healthPoints*0.1);
		Herd.neuralNetwork.setInput(2, getAttackOutput(       combatValues)*0.1);
		Herd.neuralNetwork.setInput(3, getAttackOutput(target.combatValues)*0.1);
		Herd.neuralNetwork.setInput(4, combatValues.sociability*0.1);
		
		Herd.neuralNetwork.feedForward();
		
		// Establish index of highest output
		int index = 0;
		double highestOutput = Herd.neuralNetwork.getOutput(0);
		for(int i = 1; i < 3; i++){
			double d = Herd.neuralNetwork.getOutput(i);
			if(d > highestOutput){
				highestOutput = d;
				index = i;
			}
		}
		
		// Set action based on index of output
		setActionToPerform(index);
		
		switch(enemyAction){
		case HERDING:
			movingF = true;
			groupSteering(Map.herd.agentsInHerd());
			break;

		case CHASING:
			movingF = checkDistanceToTarget();
			chaseSteering(Map.herd.agentsInHerd());
			break;

		case FLEEING:
			movingF = true;
			fleeSteering();
			break;
		}
	}
	
	void setActionToPerform(int i){
		switch(i){
		case 0:
			enemyAction = EnemyAgentAction.CHASING;
			break;
		case 1:
			enemyAction = EnemyAgentAction.FLEEING;
			break;
		case 2:
			enemyAction = EnemyAgentAction.HERDING;
			break;
		case 3:
			enemyAction = EnemyAgentAction.CHASING;
			break;
		}
	}
	
	// Flocking steering behaviour based on the herd of enemies
	void groupSteering(Agent[] neighbours){
		if(blockSteer || neighbours.length == 0) return;
		
		Vector2 averagePosition = new Vector2 (0, 0);
		Vector2 averageDirection = new Vector2 (0, 0);
		
		totalSteer = 0;
		int nearbyCount = 0;
		double partialSteer;
		int steerMultiplier = 0;
		float visionRange = 0;
		
		visionRange = Utils.getRangeFromVisionType();
		
		for(int i = 0; i < neighbours.length; i++){
			if(neighbours[i] == this|| !(neighbours[i].state == AgentState.ALIVE)) continue;
			
			//RANGE CHECK
			Vector2 dif = position.sub(neighbours[i].position);
			float distance = dif.magnitude();
			
			//Angle check
			float cosAngle = Utils.cosAngleBetweenVector(dif.newInverted(), direction);
			
			if(distance > visionRange || !Utils.isPositionInView(cosAngle)) continue;
			
			averagePosition = averagePosition.add(neighbours[i].position);
			averageDirection = averageDirection.add(neighbours[i].direction);
			nearbyCount++;
			
			/* * * * * * * * * *\
			|*    SEPARATION   *|
			\* * * * * * * * * */
			if(distance >= agentLength*Const.SEPARATION_FACTOR) continue;

			if      (dif.y < 0-Const.FRONTAREATRHESHOLD) steerMultiplier =  1;
			else if (dif.y > 0+Const.FRONTAREATRHESHOLD) steerMultiplier = -1;
			if(Utils.abs(rotation) > 1.5) steerMultiplier*= -1;//Inverts the multiplier if agent is going from right to left	
			
			separationMultiplier = distance - (agentLength*Const.SEPARATION_FACTOR); // Calculates the force of separation rule (agents try to separate harder when are closer to each other)
			
			partialSteer = steerMultiplier * separationMultiplier * Const.ROT_FORCE * ((agentLength * Const.SEPARATION_FACTOR)/distance);
			totalSteer += partialSteer;
		}
		if(nearbyCount > 0){ 
			if (!newLeader){ 	//Avoids not having a leader when all have neighbors
				leader = false; //If has neighbors its not a leader
			}
			averagePosition.div(nearbyCount); averageDirection.div(nearbyCount).normalize();

			/* * * * * * * * * *\
	    	|*    COHESION     *|
			\* * * * * * * * * */
			Vector2 u = averagePosition.sub(position); u.normalize();
			Vector2 v = direction.newNormalized();
			Vector2 w = u.rot2D(-rotation);
			steerMultiplier = 0;
			
			if      (w.y < 0) steerMultiplier = -1; 
			else if (w.y > 0) steerMultiplier =  1;	

			float dot = u.dot(v);  //Dot product
			if(dot < 1){  //The angle or/& the distance between its ends is bigger as the result of the product is lower.
				cohesionMultiplier = Utils.abs(dot - 1.0f); // The bigger the distance to the avergePos the bigger the force applied
				partialSteer = steerMultiplier * cohesionMultiplier * Const.ROT_FORCE * Math.acos(dot) / Math.PI;
				totalSteer += partialSteer;
			}

			/* * * * * * * * * *\
	    	|*    ALIGNMENT    *|
			\* * * * * * * * * */
			u = averageDirection.newNormalized();
			v = direction.newNormalized();
			w = u.rot2D(-rotation);
			steerMultiplier = 0;

			if      (w.y < 0-Const.FRONTAREATRHESHOLD) steerMultiplier = -1; 
			else if (w.y > 0+Const.FRONTAREATRHESHOLD) steerMultiplier =  1; 

			dot = u.dot(v);
			if(Utils.abs(dot) < 1){
				partialSteer = steerMultiplier * alignmentMultiplier * Const.ROT_FORCE * Math.acos(dot) / Math.PI;
				totalSteer += partialSteer;
			}
		}
		else { //If it has no neighbors
			leader = true;
			if(newLeader){
				procNewLeader(); //Makes the new leader able to stop being leader after it physically leaves the group
			}
		}
		
		if(totalSteer > Const.FTRESHOLD+0.5f) steeringR = true;
		else steeringR = false;
		if(totalSteer < Const.FTRESHOLD-0.5f) steeringL = true;
		else steeringL = false;
	}
	
	// Steering behaviour based on chasing our hostile target (player)
	void chaseSteering(Agent[] neighbours){
		if(blockSteer || target == null) return;
		
		Vector2 v = target.fixedPosition.sub(fixedPosition);
		float res = direction.x*-v.y + direction.y*v.x;
		
		if(defaultSteeringSeparation(Map.herd.agentsInHerd())){
			if(totalSteer > Const.FTRESHOLD+0.5f) steeringR = true;
			else steeringR = false;
			if(totalSteer < Const.FTRESHOLD-0.5f) steeringL = true;
			else steeringL = false;
		}
		else{
			if(res > 0 + 10){
				steeringL = true;
				steeringR = false;
			}
			else if (res < 0 - 10){
				steeringR = true;
				steeringL = false;
			}else{
				steeringR = false;
				steeringL = false;
			}
		}
	}

	// Fleeing behaviour based on running away from hostile target (player)
	void fleeSteering(){
		if(blockSteer || target == null) return;
		
		Vector2 v = target.fixedPosition.sub(fixedPosition);
		
		// If safe distance, use herding behaviour
		if (v.magnitude() >= safeDistance) {
			groupSteering(Map.herd.agentsInHerd());
			fleeHerding = true;
			return;
		}

		fleeHerding = false;
		float res = direction.x*-v.y + direction.y*v.x;
		if(res > 10){
			steeringL = false;
			steeringR = true;
		}
		else if (res < -10){
			steeringR = false;
			steeringL = true;
		}else{
			steeringR = false;
			steeringL = false;
		}
	}

	boolean defaultSteeringSeparation(Agent[] neighbours){
		Vector2 averagePosition  = new Vector2 (0, 0);
		Vector2 averageDirection = new Vector2 (0, 0);

		totalSteer = 0;
		int nearbyCount = 0;
		double partialSteer;
		int steerMultiplier = 0;
		float visionRange = Utils.getRangeFromVisionType();
		
		for(int i = 0; i < neighbours.length; i++){
			if(neighbours[i] == this|| !(neighbours[i].state == AgentState.ALIVE)) continue;
			
			//RANGE CHECK
			Vector2 dif = position.sub(neighbours[i].position);
			float distance = dif.magnitude();
			
			//Angle check
			float cosAngle = Utils.cosAngleBetweenVector(dif.newInverted(), direction);
			
			if(distance > visionRange || !Utils.isPositionInView(cosAngle)) continue;
			
			averagePosition = averagePosition.add(neighbours[i].position);
			averageDirection = averageDirection.add(neighbours[i].direction);
			nearbyCount++;
			
			/* * * * * * * * * *\
			|*    SEPARATION   *|
			\* * * * * * * * * */
			if(distance >= agentLength*Const.SEPARATION_FACTOR) continue;

			if      (dif.y < 0-Const.FRONTAREATRHESHOLD) steerMultiplier =  1;
			else if (dif.y > 0+Const.FRONTAREATRHESHOLD) steerMultiplier = -1;
			if(Utils.abs(rotation) > 1.5) steerMultiplier*= -1;//Inverts the multiplier if agent is going from right to left	
			
			separationMultiplier = distance - (agentLength*Const.SEPARATION_FACTOR); // Calculates the force of separation rule (agents try to separate harder when are closer to each other)
			
			partialSteer = steerMultiplier * separationMultiplier * Const.ROT_FORCE * ((agentLength * Const.SEPARATION_FACTOR)/distance);
			totalSteer += partialSteer;
		}
		
		if(nearbyCount > 0){
			return true;
		}
		return false;
	}
	
	// Use this boolean to check if inside of stopping range considering our combat values
	boolean checkDistanceToTarget(){
		if(target.fixedPosition.sub(fixedPosition).magnitude() <= combatValues.stopDistance) return false;
		return true;
	}
	
	public void subDraw(Graphics g, JPanel jp, AffineTransform transform) {
		Graphics2D g2d = (Graphics2D) g;
	    
		if(state == AgentState.DEATH) g2d.drawImage(image2, transform, jp);
		else{
			if(Agent.getAttackOutput(defaultCombatValues) > 3){
				g2d.drawImage(image3, transform, jp);
			}
			else if(defaultCombatValues.sociability > 4){
				g2d.drawImage(image4, transform, jp);
			}else
			g2d.drawImage(image, transform, jp);
		}
	}
	
	protected void subDrawUI(Graphics g, JPanel jp){
		if(state == AgentState.DEATH) return;
		
		Graphics2D g2d = (Graphics2D) g;
		
		int posX = (int)(fixedPosition.x - GenericImageStorage.halfIconWidth);
		int posY = (int)position.y - 14 - uiCircleRadius;
		
		switch(enemyAction){
		case HERDING:
			if (leader)
				g2d.drawImage(GenericImageStorage.leaderIcon,  posX, posY, jp);
			else
				g2d.drawImage(GenericImageStorage.herdingIcon, posX, posY, jp);
			break;

		case CHASING:
			g2d.drawImage(GenericImageStorage.aggressiveIcon, posX, posY, jp);
			break;

		case FLEEING:
			if(fleeHerding){
				if (leader)
					g2d.drawImage(GenericImageStorage.leaderIcon,  posX, posY, jp);
				else
					g2d.drawImage(GenericImageStorage.herdingIcon, posX, posY, jp);
			}
			else
				g2d.drawImage(GenericImageStorage.afraidIcon, posX, posY, jp);
			break;
		}
	}
	
	public void setDeath(){
		kill();
		leader = false;
	}
	
	public void procNewLeader(){
		hImgWidth = imgWidth/2; 
		hImgHeight = imgHeight/2;
		
		if(newLeader)	newLeader = false;
		else 			newLeader = true;
	}
}
