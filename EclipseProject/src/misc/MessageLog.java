package misc;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.text.DecimalFormat;

import main.Vector2;

public class MessageLog {

	static int messageAmount = 8;
	
	static final float lifetime = 5;
	
	static String currentMessage[] = new String[messageAmount];
	static float timer[] = new float[messageAmount];
	static Color colors[] = new Color[messageAmount];
	static int count = 0;
	
	static Vector2 pos = new Vector2(Const.WINDOW_WIDTH * 0.85f, 10);
	static DecimalFormat df = new DecimalFormat("+##,##00.00;-#");
	
	static Color boxCol = new Color(0.2f, 0.2f, 0.2f, 0.75f);
	
	static{
		for(int i = 0; i < messageAmount; i++){
			currentMessage[i] = "";
			timer[i] = 0;
			colors[i] = Color.WHITE;
		}
	}
	
	public static void recieveMessage(String s){
		currentMessage[count] = s;
		recieveSet();
	}
	
	public static void recieveMessage(float f){
		currentMessage[count] = ""+df.format(f);
		recieveSet();
	}
	
	public static void recieveMessage(int i){
		currentMessage[count] = ""+i;
		recieveSet();
	}
	
	static void recieveSet(){
		int r = 0;
		int g = 0;
		int b = 0;
		// Avoid colors too dark
		while((r+g+b) < 300){
			r = RandomGenerator.RandomInt(0, 255);
			g = RandomGenerator.RandomInt(0, 255);
			b = RandomGenerator.RandomInt(0, 255);
		}
		colors[count] = new Color(r, g, b);
				
		timer[count] = lifetime;
		count++;
		count = count >= messageAmount ? 0 : count;
	}
	
	public static void draw(Graphics g){
		g.setColor(boxCol);
		int amount = 0;
		for(int i = 0; i < messageAmount; i++){
			timer[i] -= Const.DELAY * 0.001f;
			if(timer[i] > 0) amount++;
		}
		
		g.fillRect((int)pos.x, (int)pos.y, 150, 28*amount);
		g.setFont(new Font("TimesRoman", Font.BOLD, 18));

		for(int i = 0; i < amount; i++){
			g.setColor(colors[i]);
			g.drawString(currentMessage[i],(int)pos.x+2, (int)pos.y+23*(i+1));
		}
	}
}
