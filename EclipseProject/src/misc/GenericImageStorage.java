package misc;

import java.awt.Image;

import javax.swing.ImageIcon;

public class GenericImageStorage {
	public static final float halfIconWidth = 12;
	
	public static Image aggressiveIcon;
	public static Image afraidIcon;
	public static Image herdingIcon;
	public static Image leaderIcon;
	
	static{
		RandomGenerator placeholder = new RandomGenerator();
		
		ImageIcon ii = new ImageIcon(placeholder.getClass().getResource("/resources/aggressive_icon.png"));
		aggressiveIcon = ii.getImage();
		
		ii = new ImageIcon(placeholder.getClass().getResource("/resources/afraid_icon.png"));
		afraidIcon = ii.getImage();

		ii = new ImageIcon(placeholder.getClass().getResource("/resources/herding_icon.png"));
		herdingIcon = ii.getImage();

		ii = new ImageIcon(placeholder.getClass().getResource("/resources/leader_icon.png"));
		leaderIcon = ii.getImage();
	}
}
