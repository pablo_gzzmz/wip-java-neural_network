package misc;
import agents.AgentVisionType;
import main.Vector2;

public class Utils {
	public static float clampFloat(float value, float min, float max){
		return value = value < min ? min : value > max ? max : value; 
	}
	
	
    public static float getRangeFromVisionType(){
    	switch(VISION_TYPE){
    	case WIDE:
    		return Const.WIDE_RANGE;
    	case LIMITED:
    		return Const.LIMITED_RANGE;
    	case NARROW:
    		return Const.NARROW_RANGE;
    	default:
    		return Const.WIDE_RANGE;
    	}
    }

    
    public static AgentVisionType VISION_TYPE = AgentVisionType.WIDE;
    
    public static boolean isPositionInView(float cosAngle){
    	switch(VISION_TYPE){
    	case WIDE:
    	if(cosAngle > -0.70f){
			return true;
		}
    	return false;
    	
	    case LIMITED:
		if(cosAngle > 0){
			return true;
		}
		return false;
		
    	case NARROW:
    	if(cosAngle > 0.70f){
    		return true;
    	}
    	return false;
    	
    	default:
    		return false;
    	}
    }
    
    public static float vectorsDistance (Vector2 a, Vector2 b){
    	Vector2 result = a.sub(b);
    	return result.magnitude();
    }
    
    public static float cosAngleBetweenVector(Vector2 a, Vector2 b){
    	return a.dot(b)/(a.magnitude()*b.magnitude());
    }
    
    public static float abs(float a){
    	return a = a < 0 ? -a : a;
    }
}
