package misc;

// To be used as a struct
public class CombatValues {
	public int healthPoints;
	public int attackDamage;
	
	public float combatRadius;
	public float attackDelay;

	public int sociability;
	
	public float stopDistance;
	
	public CombatValues(){
		setAttackDamage(RandomGenerator.RandomInt(0, 3));
		setHealthPoints(RandomGenerator.RandomInt(1, 4));
		setSociability (RandomGenerator.RandomInt(3, 5));
		setCombatRadius(RandomGenerator.RandomFloat(50, 80));
		setAttackDelay (1f);
	}
	
	public CombatValues(CombatValues other){
		setHealthPoints(other.healthPoints);
		setAttackDamage(other.attackDamage);
		setSociability (other.sociability);
		setCombatRadius(other.combatRadius);
		setAttackDelay (other.attackDelay);
		
	}
	
	public CombatValues(int _healthPoints, int _attackDamage, int _sociablility, float _combatRadius, float _attackDelay){
		setAttackDamage(_attackDamage);
		setHealthPoints(_healthPoints);
		setSociability (_sociablility);
		setCombatRadius(_combatRadius);
		setAttackDelay (_attackDelay);
	}
	
	public void changeValues(CombatValues other){
		setAttackDamage(other.attackDamage);
		setHealthPoints(other.healthPoints);
		setSociability (other.sociability);
		setCombatRadius(other.combatRadius);
		setAttackDelay (other.attackDelay);
	}
	
	public void setAttackDamage(int d){
		attackDamage = d < 0 ? 0 : d > 9 ? 9 : d;
	}
	
	public void setHealthPoints(int h){
		healthPoints = h < 1 ? 1 : h > 9 ? 9 : h;
	}
	
	public void setSociability(int s){
		sociability = s < 0 ? 0 : s > 10 ? 9 : s;
	}
	
	public void setCombatRadius(float f){
		combatRadius = f < 0 ? 0 : f;
		stopDistance = combatRadius - 4;
	}
	
	public void setAttackDelay(float f){
		attackDelay = f < 0.1f ? 0.1f : f > 2 ? 2 : f;
	}
}
