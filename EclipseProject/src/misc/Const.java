package misc;
import java.awt.Color;

public class Const {
	public static final int WINDOW_WIDTH = 1600;
	public static final int WINDOW_HEIGHT = 900;
    public static final int DELAY = 5;
	public static final float FTRESHOLD = 0.001f;
	public static final Color COUNT_DOWN_COLOR = Color.BLACK;
	
	public static final float ROT_ANGLE = 0.03f;
	
	public static final float FRONTAREATRHESHOLD = ROT_ANGLE * 0.7f;
    
    public static final float ROT_FORCE = 6.0f;
    
    public static final float WIDE_RANGE = 150;
    public static final float LIMITED_RANGE = 150;
    public static final float NARROW_RANGE = 150;
    
    public static final float BACK_ANGLE_FACTOR = 1; //45�
    public static final float FORWARD_ANGLE_FACTOR = 1; //45�
    
    public static final float INITIAL_AGENT_SEPARATION = 40; //In pixels
    public static final float OBSTACLE_GENERATION_SEPARATION = 200; //In pixels
    
    public static final float SEPARATION_FACTOR = 0.70f;  //Separation between agents (in length of agents)

    public static final float RESPAWN_DELAY_SECONDS = 5f;
    public static final int HERD_MEMBERS = 6;
    
    // NEURAL NETWORK CONSTANTS
	public static final float 	LEARNING_RATIO    = 0.3f;
	public static final float 	INERTIA_FACTOR	  = 0.5f;
	public static final boolean LINEAL_OUTPUT 	  = false;
	public static final boolean USING_INERTIA 	  = true;
	public static final double	OUTPUT_ERROR	  = 999999.0f;
	
	// WEIGHTS MATRIXES
	
	public static double[][] WEIGHTSNOTUSED1 = {
			//		   	 0		 1 		  2 		 3 			 4 
			/*  0 */{ 	 0.80, 	 0.56, 	 -0.79,		-0.05,		-0.23},
			/*  1 */{ 	 0.67, 	 0.06, 	  0.62, 	 0.56,		 0.31},
			/* 	2 */{ 	-0.64, 	-0.41, 	 -0.70, 	 0.48,		 0.80},
			/* 	3 */{ 	 0.58, 	-0.44, 	 -0.49, 	-0.81,		 0.72},
			/*	4 */{ 	-0.49, 	 0.63, 	  0.87, 	-0.76,		 1.00}
	};	
	
	public static double[][] WEIGHTSNOTUSED2 = {
			//		   	 0		 1 		  2 		 
			/*  0 */{ 	-0.52, 	 0.37, 	 -0.27},
			/*  1 */{ 	-0.76, 	 0.96, 	 -0.69},
			/* 	2 */{ 	-0.33, 	-0.98, 	 -0.35},
			/* 	3 */{ 	 0.10, 	 0.69, 	 -0.62},
			/*	4 */{ 	-0.02, 	 0.76, 	  0.67}
	};
	
	public static double[][] WEIGHTS1 = {
			//		   	 0		 1 		  2 		 3 			 4 
			/*  0 */{ 	 1.14, 	 0.73, 	 -1.09,		-0.08,		-0.38},
			/*  1 */{ 	 0.98, 	 0.05, 	  1.01, 	 0.90,		 0.43},
			/* 	2 */{ 	-1.03, 	-0.74, 	 -0.94, 	 0.73,		 1.15},
			/* 	3 */{ 	 0.86, 	-0.67, 	 -0.70, 	-1.17,		 1.05},
			/*	4 */{ 	-0.77, 	 0.88, 	  1.39, 	-1.10,		 1.45}
	};	
	
	public static double[][] WEIGHTS2 = {
			//		   	 0		 1 		  2 		 
			/*  0 */{ 	-0.57, 	 0.45, 	 -0.59},
			/*  1 */{ 	-0.91, 	 1.29, 	 -1.22},
			/* 	2 */{ 	-0.14, 	-1.64, 	 -0.84},
			/* 	3 */{ 	 0.50, 	 0.81, 	 -1.21},
			/*	4 */{ 	 0.42, 	 0.86, 	  0.64}
	};
	
	//public static double[] BIAS1 = {0.80, 0.64, -0.72, -0.75, 0.17};	
	//public static double[] BIAS2 = {0.82, 0.87, -0.83};	
	
	public static double[] BIAS1 = {0.85, 0.74, -0.83, -0.80, 0.23};	
	public static double[] BIAS2 = {0.44, 1.10, -0.52 };	
	
	
	
	// GENETIC ALGORITHMS
	public static final int RADIUS_MUTABILITY = 1; 			// x in 10 times the radius mutates randomly
	public static final int ATTACK_DELAY_MUTABILITY = 1; 	// x in 10 times the attack delay mutates randomly
	public static final int DAMAGE_MUTABILITY = 1; 			// x in 10 times the attack damage mutates randomly
	public static final int HP_MUTABILITY = 1; 				// x in 10 times the health mutates randomly
	public static final int SOCIAL_MUTABILITY = 1; 			// x in 10 times the sociability mutates randomly
	public static final int NUMBER_AGENTS_TO_PROCREATE = 3;
	
   
}
