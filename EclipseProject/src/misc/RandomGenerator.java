package misc;
import java.util.Random;

public class RandomGenerator {

	static Random rand = new Random();
	
	public static int RandomInt(int min, int max)
	{
		int randomNum = rand.nextInt((max - min) + 1) + min;
	    return randomNum;
	}
	
	public static float RandomFloat()
	{
		float randomNum = rand.nextFloat();
	    return randomNum;
	}
	
	public static float RandomFloat(float min, float max){
		float randomNum = rand.nextFloat() * (max - min) + min;
	    return randomNum;
	}
}
